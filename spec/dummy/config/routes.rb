Rails.application.routes.draw do
  mount Chat::Engine, at: "/chat", as: 'chat'
end
