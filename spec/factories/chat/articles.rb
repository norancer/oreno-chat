FactoryGirl.define do
  factory :init_article, class: 'Chat::Article' do
    title 'sample title'
    text 'sample text'
  end
end
