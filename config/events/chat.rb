WebsocketRails::EventMap.describe do
  namespace :chat do
    subscribe :new_message, to: Chat::SocketsController, with_method: :new_message
  end
end
