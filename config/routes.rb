Chat::Engine.routes.draw do
  # get 'chat/index'
  # root to: "chat#index"
  get '/chat' => 'templates#index'
  get '/templates/:path.html' => 'templates#template', constraints: { path: /.+/ }
  root 'templates#index'

  namespace :api, defaults: { format: :json } do
    resources :rooms, except: [:new, :edit] do
      resources :messages, except: [:new, :edit, :show]
    end
  end
end
