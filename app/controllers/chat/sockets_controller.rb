module Chat
  class SocketsController < WebsocketRails::BaseController
    def new_message
      # todo: Change the specification .
      @message = Chat::RoomMessage.new
      @message.text = message[:text]
      Chat::Filter.new(@message).call
      @message.save!
      broadcast_message :new_message, @message,
                        namespace: :chat
    end
  end
end
