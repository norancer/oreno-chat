class Chat::TemplatesController < Chat::ApplicationController
  def index
  end

  def template
    render template: "chat/templates/#{params[:path]}", layout: nil
  end
end
