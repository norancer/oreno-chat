# Chat::Api::MessagesController
module Chat
  class Api::MessagesController < ApplicationController
    def index
      @messages = Chat::RoomMessage.all
      @count = Chat::RoomMessage.count
    end
  end
end
