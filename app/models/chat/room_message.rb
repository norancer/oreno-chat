module Chat
  class RoomMessage < ActiveRecord::Base
    def remark
      self.text
    end

    def reply
      self.text
    end

    def reply=(value)
      self.text = value
    end
  end
end
