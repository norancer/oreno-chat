angular.module('chatApp').controller "ChatCtrl", ($scope, ChatRoomMessage) ->

  $scope.init = ->
    @messageService = new ChatRoomMessage(1, serverErrorHandler)
    $scope.lists = @messageService.all()
    @dispatcher = new WebSocketRails($('#url').val(), false)
    @dispatcher.bind 'chat.new_message', receiveMessage

  $scope.sendMessage = (text) ->
    @dispatcher.trigger 'chat.new_message', { text: text }

  receiveMessage = (message) ->
    $scope.lists.push(message)
    $scope.text = ""
    $scope.$apply()

  serverErrorHandler = ->
    alert("Server error")
