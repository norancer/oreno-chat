angular.module('chatApp').factory 'ChatRoomMessage', ($resource, $http) ->
  class ChatRoomMessage
    constructor: (RoomId, errorHandler) ->
      @service = $resource('/chat/api/rooms/:room_id/messages',
        { room_id: RoomId })
      @errorHandler = errorHandler

    all: ->
      @service.query((-> null), @errorHandler)
