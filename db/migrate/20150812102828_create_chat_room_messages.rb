class CreateChatRoomMessages < ActiveRecord::Migration
  def change
    create_table :chat_room_messages do |t|
      t.string :text

      t.timestamps null: false
    end
  end
end
