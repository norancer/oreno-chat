require 'chat/filter'
require 'chat/command'

module Chat
  module Handlers
    class Base
      @@commands = []

      def commands
        @@commands
      end 

      def call(message)
        @@commands.each do |cmd|
          cmd.call(self, message)
        end
      end

      def self.hook(opts={})
        @@commands << Chat::Command.new(opts)
      end

      def self.inherited(child)
        Chat::Filter.add_handler child
      end
    end
  end
end
