# lib::Chat
module Chat
  # lib::Chat::Engine
  class Engine < ::Rails::Engine
    isolate_namespace Chat

    config.autoload_paths << File.expand_path("../../../lib", __FILE__)

    initializer 'Chat.factories', after: 'factory_girl.set_factory_paths' do
      FactoryGirl.definition_file_paths.unshift File
        .expand_path('../../../spec/factories', __FILE__)
    end
  end
end
