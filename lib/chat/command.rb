module Chat
  class Command
    def initialize(opts={})
      @pattern = opts[:pattern]
      @name = opts[:name]
      @description = opts[:description]
    end

    def call(handler, message)
      handler.send(@name.to_s, message)
    end
  end
end
