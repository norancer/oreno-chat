module Chat
  module Commands
    class Base
      attr_accessor :message
      
      def initialize(message)
        @message = message
      end
    end
  end
end
