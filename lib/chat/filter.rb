module Chat
  class Filter
    @@handlers = []

    def initialize(message)
      @message = message
    end

    def handlers
      @@handlers.map do |handler|
        handler.new
      end
    end 

    def self.add_handler(value)
      @@handlers << value
    end 

    def call
      handlers.each do |handler|
        handler.call(@message)
      end
    end
  end
end
